package library.entities;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import library.entities.helpers.IBookHelper;
import library.entities.helpers.ILoanHelper;
import library.entities.helpers.IPatronHelper;

@ExtendWith(MockitoExtension.class)
class LibraryTest {
	
	Date dueDate;	
	SimpleDateFormat formatter;
	@Mock ILoan mockLoan;
	@Mock ICalendar mockCalendar;
	@Mock ILoanHelper mockLoanHelper;
	@Mock IPatronHelper mockPatronHelper;
	@Mock IBookHelper mockBookHelper;
	
	@InjectMocks
	ILibrary library = new Library(mockBookHelper, mockPatronHelper, mockLoanHelper);
	

	@BeforeEach
	void setUp() throws Exception {
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	void testOverDueByOneDay() {
		double expectedFine = 1.0;
		formatter = new SimpleDateFormat("dd-MM-yyyy");
		try {
			//current date "19-10-2020"
			dueDate = formatter.parse("18-10-2020");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		when(mockLoan.isOverDue()).thenReturn(true);
		when(mockLoan.getDueDate()).thenReturn(dueDate);
		//act
		double actualFine = library.calculateOverDueFine(mockLoan);
		//assert
		assertEquals(expectedFine, actualFine);
	}
	
	@Test
	void testOverDueByOneTwo() {
		double expectedFine = 2.0;
		formatter = new SimpleDateFormat("dd-MM-yyyy");
		try {
			//current date "19-10-2020"
			dueDate = formatter.parse("17-10-2020");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		when(mockLoan.isOverDue()).thenReturn(true);
		when(mockLoan.getDueDate()).thenReturn(dueDate);
		//act
		double actualFine = library.calculateOverDueFine(mockLoan);
		//assert
		assertEquals(expectedFine, actualFine);
	}

}
